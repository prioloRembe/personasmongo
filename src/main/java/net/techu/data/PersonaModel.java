package net.techu.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("personas")
public class PersonaModel {

    @Id
    public String id;
    public String nombre;
    public String apellido;
    public double salario;
    public boolean activo;

    public PersonaModel() {
    }

    public PersonaModel(String id, String nombre, String apellido, double salario, boolean activo) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.salario = salario;
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", salario=" + salario +
                ", activo=" + activo +
                '}';
    }
}
